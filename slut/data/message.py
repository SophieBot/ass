import datetime

from aiogram.filters import CommandObject
from aiogram.types import Message, MessageEntity

from slut.data.chat import TEST_CHAT
from slut.data.user import TEST_USER

TEST_TEXT_MESSAGE = Message(  # type: ignore
    message_id=333,
    from_user=TEST_USER,  # type: ignore
    date=datetime.datetime.now(),
    chat=TEST_CHAT,
    text="foo bar"
)


class TestMessage:
    data: Message

    def __init__(self, **overwrite):
        fields = TEST_TEXT_MESSAGE.dict().copy()
        fields.update(overwrite)
        self.data = Message(**fields)

    def cmd(self, prefix: str = '!cmd') -> CommandObject:
        return CommandObject(
            prefix=prefix[0],
            command=prefix[1:],
            args=(self.data.text or '').removeprefix(f'{prefix} '),
        )


TEST_ENTITIES_MESSAGE = TestMessage(
    text="normal bold strikethrough italic underlined spoiler code url multientity",
    entities=[
        MessageEntity(offset=7, length=4, type="bold"),
        MessageEntity(offset=12, length=13, type="strikethrough"),
        MessageEntity(offset=26, length=6, type="italic"),
        MessageEntity(offset=33, length=10, type="underline"),
        MessageEntity(offset=44, length=7, type="spoiler"),
        MessageEntity(offset=52, length=4, type="code"),
        MessageEntity(offset=57, length=3, type="text_link", url="http://google.com/"),
        MessageEntity(offset=61, length=11, type="bold"),
        MessageEntity(offset=61, length=11, type="italic"),
        MessageEntity(offset=61, length=11, type="underline"),
        MessageEntity(offset=61, length=11, type="strikethrough"),
        MessageEntity(offset=61, length=11, type="spoiler")
    ]
)
