from aiogram.types import User


class TestUser(User):
    def __init__(self, dataset: User, **overwrite):
        super().__init__(**overwrite, **dataset.dict())


TEST_USER = User(
    id=123,
    is_bot=False,
    first_name="Foo Bar"
)

TEST_ANON_BOT = User(
    id=1087968824,
    username="GroupAnonymousBot",
    first_name="Group",
    is_bot=True
)

TEST_TELEGRAM_BOT = User(
    id=777000,
    first_name='Telegram',
    is_bot=False,
)
