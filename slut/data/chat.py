from aiogram.types import Chat


class TestChat(Chat):
    def __init__(self, dataset: Chat, **overwrite):
        super().__init__(**overwrite, **dataset.dict())


TEST_CHAT = Chat(
    id=123,
    type='supergroup',
    title='FooBar'
)
