from datetime import timedelta

from aiogram import Bot, Dispatcher, flags
from aiogram.client.default import DefaultBotProperties
from aiogram.filters import Command
from aiogram.types import Message
from stfu_tg import Section, KeyValue

from ass_tg.entities import ArgEntities
from ass_tg.middleware import ArgsMiddleware
from ass_tg.types import ActionTimeArg, TextArg, OptionalArg, UserArg

bot = Bot(token="5165382377:AAG0HpE6zhczMgxUj2DsnxsdVuwAwlGDies", default=DefaultBotProperties(parse_mode="html"))
dp = Dispatcher()

# Ensure to add arguments parser middleware!
dp.message.middleware(ArgsMiddleware())


class ShortTextArg(TextArg):
    async def parse(self, text: str, offset: int, entities: ArgEntities) -> tuple[int, str]:
        length, text = super().parse(text, offset, entities)

        if len(text) > 10:
            text = text[:10] + "..."

        return length, text


@dp.message(Command('tban'))  # Set a command filter
@flags.args(
    user=UserArg("User"),
    time=ActionTimeArg("Time to ban"),
    description=OptionalArg(ShortTextArg("Reason text"))
)
async def tban_user_handler(
        msg: Message,
        user: str,
        time: timedelta,
        description: str | None
):
    # Here we used STFU's formatting, but you can use variables as you wish!
    await msg.reply(str(Section(
        KeyValue("User", user),
        KeyValue("On", time),
        KeyValue("Description", description or "No description"),
        title="Ban"
    )))


dp.run_polling(bot)
