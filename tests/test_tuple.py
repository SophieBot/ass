import pytest

from ass_tg.types import TextArg, WordArg, AndArg
from tests.data import NO_ENTITIES


@pytest.mark.asyncio
async def test_tuple_arg():
    arg = await AndArg(
        one=WordArg(),
        second=TextArg()
    )('Example foo bar', 5, NO_ENTITIES)
    assert arg.value['one'].value == 'Example'
    assert arg.value['second'].value == 'foo bar'
