import pytest

from ass_tg.entities import ArgEntities
from ass_tg.types import TextArg
from slut.data.message import TEST_ENTITIES_MESSAGE


@pytest.mark.asyncio
async def test_textarg_entities():
    msg = TEST_ENTITIES_MESSAGE
    entities = ArgEntities(msg.data.entities)  # type: ignore

    arg = await TextArg(parse_entities=True)(str(msg.data.text), 0, entities)

    assert arg.value == 'normal <b>bold</b> <s>strikethrough</s> <i>italic</i> <u>underlined</u>' \
                        ' <tg-spoiler>spoiler</tg-spoiler> <code>code</code> <a href="http://google.com/">url</a> ' \
                        '<b><i><u><s><tg-spoiler>multientity</tg-spoiler></s></u></i></b>'

# @pytest.mark.asyncio
# async def test_quotedtext_entities():
#     msg = TestMessage(
#         text='"normal bold strikethrough italic" underlined spoiler code url multientity',
#         entities=[
#             MessageEntity(offset=8, length=4, type="bold"),
#             MessageEntity(offset=13, length=13, type="strikethrough"),
#             MessageEntity(offset=27, length=6, type="italic"),
#             MessageEntity(offset=34, length=10, type="underline"),
#             MessageEntity(offset=45, length=7, type="spoiler"),
#             MessageEntity(offset=53, length=4, type="code"),
#             MessageEntity(offset=58, length=3, type="text_link", url="http://google.com/"),
#             MessageEntity(offset=62, length=11, type="bold"),
#             MessageEntity(offset=62, length=11, type="italic"),
#             MessageEntity(offset=62, length=11, type="underline"),
#             MessageEntity(offset=62, length=11, type="strikethrough"),
#             MessageEntity(offset=62, length=11, type="spoiler")
#         ]
#     )
#     entities = ArgEntities(msg.data.entities)
#
#     arg = await SurroundedArg(TextArg(parse_entities=True))(msg.data.text, 0, entities)
#
#     # Known to be broken
#     # assert arg.value == 'normal <b>bold</b> <s>strikethrough</s> <i>italic</i>'
