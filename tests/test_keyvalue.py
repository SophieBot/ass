import pytest

from ass_tg.exceptions import ArgCustomError, ArgInListItemError
from ass_tg.types import KeyValueArg, BooleanArg, WordArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('^keyname=FooBar', 'FooBar'),
    ('^    keyname=FooBar', 'FooBar'),
    ('^keyname    =FooBar', 'FooBar'),
    ('^keyname=    FooBar', 'FooBar'),
    ('^  keyname  =    FooBar', 'FooBar'),
])
@pytest.mark.asyncio
async def test_keyvalue_arg(text: str, result: str):
    arg = await KeyValueArg('keyname', WordArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)
    assert arg.offset == 5


@pytest.mark.asyncio
async def test_keyvalue_arg_must_contain_value():
    with pytest.raises(ArgCustomError):
        await KeyValueArg('keyname', WordArg())("^keyname", 5, NO_ENTITIES)


@pytest.mark.parametrize('text,result', [
    ('^keyname', True),
    ('^keyname=False', False),
    ('^keyname =    false', False),
    ('^keyname=True', True),
    ('^keyname=:(', False),
])
@pytest.mark.asyncio
async def test_keyvalue_arg_can_contain_no_value_or_value(text: str, result: bool):
    arg = await KeyValueArg('keyname', BooleanArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)
    assert arg.offset == 5


@pytest.mark.asyncio
async def test_keyvalue_raise_arg_error():
    with pytest.raises(ArgInListItemError):
        await KeyValueArg('keyname', BooleanArg())("^keyname=foobar", 5, NO_ENTITIES)
