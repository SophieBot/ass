import pytest

from ass_tg.exceptions import ArgTypeError
from ass_tg.types.oneword import BooleanArg, IntArg, WordArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('Hello', 'Hello'),
    ('Foo bar', 'Foo'),
    ('123', '123'),
])
@pytest.mark.asyncio
async def test_word_arg(text: str, result: str):
    arg = await WordArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(result)


@pytest.mark.parametrize('text,result', [
    ('123', 123),
    ('-1', -1),
    ('0', 0),
])
@pytest.mark.asyncio
async def test_int_arg(text: str, result: str):
    arg = await IntArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    '0,9999',
    '0.9999',
    'WRONG_VALUE'
])
@pytest.mark.asyncio
async def test_int_arg_fail(text: str):
    with pytest.raises(ArgTypeError):
        await IntArg()(text, 5, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    'True',
    'true',
    't',
    'y',
    'yes',
    ':)',
    '1',
    'enable',
    'enabled',
    '+'
])
@pytest.mark.asyncio
async def test_boolean_arg(text: str):
    arg = await BooleanArg()(text, 5, NO_ENTITIES)

    assert arg.value is True
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    'False',
    'false',
    'f',
    'n',
    'no',
    ':(',
    '0',
    'disable',
    'disabled',
    '-'
])
@pytest.mark.asyncio
async def test_boolean_arg_false(text: str):
    arg = await BooleanArg()(text, 5, NO_ENTITIES)

    assert arg.value is False
    assert arg.length == len(text)
    assert arg.offset == 5


@pytest.mark.parametrize('text', [
    'Nonsense',
    "Yesn't",
    'WRONG_VALUE'
])
@pytest.mark.asyncio
async def test_boolean_arg_fail(text: str):
    with pytest.raises(ArgTypeError):
        await BooleanArg()(text, 5, NO_ENTITIES)
