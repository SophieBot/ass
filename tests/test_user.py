import pytest
from aiogram.types import MessageEntity, User
from aiogram.utils.i18n import I18n

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ArgTypeError
from ass_tg.i18n import gettext_ctx
from ass_tg.types.user import UserIDArg, UsernameArg, UserMentionArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ("12345", 12345),
    ("123456", 123456),
    ("777000", 777000)
])
@pytest.mark.asyncio
async def test_user_id(text: str, result: int):
    arg = await UserIDArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    "12345a",
    "abcdeg",
    "a777000",
])
@pytest.mark.asyncio
async def test_user_id_wrong(text: str):
    with pytest.raises(ArgTypeError):
        await UserIDArg()(text, 5, NO_ENTITIES)


@pytest.mark.parametrize('text,result', [
    ("@username", "username"),
    ("@foo", "foo"),
    ("@telegram", "telegram")
])
@pytest.mark.asyncio
async def test_user_name(text: str, result: int):
    arg = await UsernameArg()(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    "12345a",
    "@dfjisdfhsdghudfhgdflhgufdhguidfhguidfhguidfhguid",
    "@",
])
@pytest.mark.asyncio
async def test_user_name_wrong(text: str):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgTypeError):
            await UsernameArg()(text, 5, NO_ENTITIES)


@pytest.mark.asyncio
async def test_user_mention():
    text = "Test mention text after"
    entities = ArgEntities(entities=[
        MessageEntity(type="bold", offset=5, length=12),
        MessageEntity(type="italic", offset=5, length=1),
        MessageEntity(type="mention", offset=5, length=12, user=User(
            first_name="Test mention", id=777000, is_bot=False
        )),
        MessageEntity(type="bold", offset=7, length=12),

    ])
    arg = await UserMentionArg()(text, offset=5, entities=entities)
    assert arg.value.id == 777000
    assert arg.length == 12


@pytest.mark.asyncio
async def test_user_mention_wrong():
    text = "Test mention text after"
    entities = ArgEntities(entities=[
        MessageEntity(type="mention", offset=6, length=12, user=User(
            first_name="Test mention", id=777000, is_bot=False
        ))
    ])

    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgTypeError):
            await UserMentionArg()(text, offset=5, entities=entities)

        with pytest.raises(ArgTypeError):
            await UserMentionArg()(text, offset=5, entities=NO_ENTITIES)
