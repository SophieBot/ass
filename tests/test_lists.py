import pytest
from aiogram.types import MessageEntity
from aiogram.utils.i18n import I18n

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import (ArgCustomError, ArgInListItemError, ArgIsRequiredError, ArgTypeError,
                               ArgSimpleTypeError)
from ass_tg.i18n import gettext_ctx
from ass_tg.types import DividedArg, ListArg, OrArg, IntArg, WordArg, TextArg, AndArg, SurroundedArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('(FooBar, TestWord, AnotherWord)', ('FooBar', 'TestWord', 'AnotherWord')),
    ('(FooBar, TestWord, AnotherWord,)', ('FooBar', 'TestWord', 'AnotherWord')),
    ('(FooBar,     TestWord   ,    AnotherWord    ,LastWord)', ('FooBar', 'TestWord', 'AnotherWord', 'LastWord')),
    ('(FooBar)', ('FooBar',)),
    ('(Foo,Bar,Bar)', ('Foo', 'Bar', 'Bar'))
])
@pytest.mark.asyncio
async def test_list_arg_with_word_arg(text: str, result: tuple[str]):
    arg = await ListArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.asyncio
async def test_list_entities():
    text = '(Hello, Foo,,,,,,, Bar)'
    data = ('Hello', 'Foo,,,,,,', 'Bar')
    entities = ArgEntities([
        MessageEntity(type='bold', offset=8, length=9),
    ])
    arg = await ListArg(WordArg())(text, 0, entities)
    assert arg.values == data


@pytest.mark.asyncio
async def test_divided_in_list_entities():
    text = '(    Hello, world!|    Woof,    Foo)'
    entities = ArgEntities([
        MessageEntity(type='bold', offset=1, length=12),
    ])
    arg = await ListArg(DividedArg(TextArg()))(text, 0, entities)
    result = (('Hello, world!', 'Woof'), ('Foo',))
    assert tuple(tuple(r.value for r in x) for x in arg.values) == result


@pytest.mark.parametrize('text,result', [
    ('((Foo, Bar), (Bar, Foo))', (("Foo", "Bar"), ("Bar", "Foo")))
])
@pytest.mark.xfail  # Expected to fail
@pytest.mark.asyncio
async def test_list_arg_folded(text: str, result: tuple[str]):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        arg = await ListArg(ListArg(WordArg()))(text, 5, NO_ENTITIES)

    assert tuple(tuple(r.value for r in x) for x in arg.values) == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('(Foo, Bar) | (Bar, Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
    ('(Foo,  Bar) |    (Bar, Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
    ('(   Foo,  Bar)    | (Bar,    Foo   )', (("Foo", "Bar"), ("Bar", "Foo"))),
])
@pytest.mark.asyncio
async def test_list_in_divided_arg(text: str, result: tuple[str]):
    arg = await DividedArg(ListArg(WordArg()))(text, 5, NO_ENTITIES)

    assert tuple(tuple(r.value for r in x) for x in arg.values) == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('(Foo | Bar, Bar | Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
    ('(Foo |    Bar, Bar | Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
    ('(Foo     | Bar, Bar | Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
    ('(Foo    |     Bar   ,     Bar  |    Foo)', (("Foo", "Bar"), ("Bar", "Foo"))),
])
@pytest.mark.asyncio
async def test_divided_in_list_arg(text: str, result: tuple[str]):
    arg = await ListArg(DividedArg(WordArg()))(text, 5, NO_ENTITIES)

    assert tuple(tuple(r.value for r in x) for x in arg.values) == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result,options', [
    ('[FooBar, TestWord]', ('FooBar', 'TestWord'), {'prefix': '[', 'postfix': ']'}),
    ('(FooBar | TestWord | Foo)', ('FooBar', 'TestWord', 'Foo'), {'separator': '|'}),
    ('FooBar | TestWord | Foo', ('FooBar', 'TestWord', 'Foo'), {'separator': '|', 'prefix': '', 'postfix': ''}),
])
@pytest.mark.asyncio
async def test_list_custom_syntax(text: str, result: tuple[str], options: dict):
    arg = await ListArg(WordArg(), **options)(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    '| ',
    '|  | ||',
    ' | | '
])
@pytest.mark.asyncio
async def test_list_no_syntax_exception(text: str):
    with pytest.raises(ArgIsRequiredError):
        await ListArg(WordArg(), separator='|', prefix='', postfix='')(text, 5, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    '(FooBar, Dar,,)',
    '(FooBar,, Dar)',
    '(,FooBar, Dar)',
    # '(FooBar, Dar,)',
])
@pytest.mark.asyncio
async def test_list_double_separator_error(text: str):
    with pytest.raises(ArgIsRequiredError):
        await ListArg(WordArg())(text, 5, NO_ENTITIES)


@pytest.mark.asyncio
async def test_list_arg_empty():
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgTypeError):
            await ListArg(WordArg())("()", 5, NO_ENTITIES)


@pytest.mark.asyncio
async def test_list_child_item_exception():
    with pytest.raises(ArgInListItemError):
        await ListArg(IntArg())("(foo)", 5, NO_ENTITIES)


@pytest.mark.parametrize('text,result', [
    ('Foo', ('Foo',)),
    ('Foo | Bar | Dar', ('Foo', 'Bar', 'Dar')),
    ('Foo |Bar| Dar', ('Foo', 'Bar', 'Dar')),
    ('Foo|    Bar|   Dar', ('Foo', 'Bar', 'Dar')),
])
@pytest.mark.asyncio
async def test_divided_arg(text: str, result: tuple[str]):
    arg = await DividedArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo" | "Bar" | "Dar"', ('Foo', 'Bar', 'Dar')),
    ('"|Foo|" | "Bar" | "Dar"', ('|Foo|', 'Bar', 'Dar')),
    ('"Foo" | "Bar" | "|Dar|"', ('Foo', 'Bar', '|Dar|')),
    ('"Foo"     |    "Bar"  |   "|Dar|"', ('Foo', 'Bar', '|Dar|')),
    ('"Foo"     |    "Bar"  |   "  |  Dar | "', ('Foo', 'Bar', '|  Dar | ')),
])
@pytest.mark.asyncio
async def test_know_the_end_child(text: str, result: tuple[str]):
    arg = await DividedArg(SurroundedArg(TextArg()))(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo" | "Bar"', ('Foo', 'Bar')),
    ('Foo| Bar', ('Foo', 'Bar')),
    ('Foo|Bar', ('Foo', 'Bar')),
    ('Foo    |Bar', ('Foo', 'Bar')),
    ('Foo|    Bar', ('Foo', 'Bar')),
    ('Foo    |    Bar', ('Foo', 'Bar')),
    ('Foo| "Bar"', ('Foo', 'Bar')),
    ('"Foo" | Bar', ('Foo', 'Bar')),
    ('"Foo" | "Bar', ('Foo', '"Bar')),
    ('Foo" | "Bar"', ('Foo"', 'Bar')),
])
@pytest.mark.asyncio
async def test_list_of_ores(text: str, result: tuple[str]):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        arg = await DividedArg(OrArg(SurroundedArg(TextArg()), WordArg()))(text, 5, NO_ENTITIES)

    assert arg.values == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('"Foo"foobar | "Bar"', ('Foo', 'Bar'))
])
@pytest.mark.asyncio
async def test_list_known_end_but_item_next(text: str, result: tuple[str]):
    with pytest.raises(ArgCustomError):
        await DividedArg(SurroundedArg(TextArg()))(text, 5, NO_ENTITIES)


@pytest.mark.parametrize(
    'text,kwargs', [
        ('(Foo, Bar', {}), ('Foo, Bar)', {}), ('(Foo, Bar)', dict(prefix='/')), ('(Foo, Bar)', dict(postfix='/'))
    ])
@pytest.mark.asyncio
async def test_list_no_syntax_symbol(text: str, kwargs: dict):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgSimpleTypeError):
            ListArg(TextArg(), **kwargs).check(text, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    "Foo|Bar Dar",
    "Foo |Bar Dar",
    "Foo| Bar Dar",
    "Foo | Bar Dar",
    "Foo     |     Bar Dar",
])
@pytest.mark.asyncio
async def test_divided_not_known_end_but_in_andarg(text: str):
    result_first = ('Foo', 'Bar')
    result_second = 'Dar'

    arg = await AndArg(
        first=DividedArg(WordArg()),
        second=WordArg(),
    )(text, 5, NO_ENTITIES)

    assert arg.value['first'].values == result_first
    assert arg.value['second'].value == result_second
    assert arg.length == len(text)
