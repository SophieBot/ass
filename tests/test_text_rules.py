import pytest
from aiogram.types import MessageEntity
from aiogram.utils.i18n import I18n

from ass_tg.entities import ArgEntities
from ass_tg.exceptions import ArgCustomError, ArgSimpleTypeError, ArgSyntaxEntityError
from ass_tg.i18n import gettext_ctx
from ass_tg.types import TextArg, WordArg, AndArg, OptionalArg
from ass_tg.types.text_rules import UntilArg, StartsWithArg, SurroundedArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('"Hello"', 'Hello'),
    ('"    Hello"', 'Hello'),
    ('"Hello     "', 'Hello'),
    ('"   Hello    "', 'Hello')
])
@pytest.mark.asyncio
async def test_surrounded_arg_word_arg(text: str, result: str):
    arg = await SurroundedArg(WordArg())(text, 0, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text', [
    '"FooBar"',
    '"    FooBar"',
    '"FooBar    "',
    '"    FooBar   "',

])
@pytest.mark.asyncio
async def test_surrounded_arg_word_check_on(text: str):
    assert SurroundedArg(WordArg()).check(text, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    '"FooBar',
    '    FooBar"',
    'FooBar    "',
    '"    FooBar   ',

])
@pytest.mark.asyncio
async def test_surrounded_arg_word_check_fail(text: str):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgSimpleTypeError):
            SurroundedArg(WordArg()).check(text, NO_ENTITIES)


@pytest.mark.asyncio
async def test_surrounded_arg_word_arg_double():
    text = '"   Foo    ""  bar "'

    arg = await AndArg(
        first=SurroundedArg(WordArg()),
        second=SurroundedArg(WordArg())
    )(text, 0, NO_ENTITIES)

    assert arg.value['first'].value == 'Foo'
    assert arg.value['second'].value == 'bar'

    assert arg.length == len(text)


@pytest.mark.asyncio
async def test_surrounded_arg_entity():
    text = '"Link to the "Google.com"     "'
    entities = ArgEntities([MessageEntity(type='bold', offset=1, length=24)])
    arg = await SurroundedArg(TextArg())(text, 0, entities)

    result = 'Link to the "Google.com"     '
    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.asyncio
async def test_surrounded_arg_entity_set_arg():
    text = '"Foo "Bar" "      ""second arg "Dar" "'

    arg = await AndArg(
        first=SurroundedArg(TextArg()),
        second=SurroundedArg(TextArg())
    )(text, 0, ArgEntities([
        MessageEntity(type='text_link', offset=1, length=9),
        MessageEntity(type='underline', offset=19, length=11),
        MessageEntity(type='italic', offset=31, length=5),
    ]))

    assert arg.value['first'].value == 'Foo "Bar" '
    assert arg.value['second'].value == '"second arg "Dar" '


@pytest.mark.asyncio
async def test_surrounded_arg_entity_overlap():
    text = '"Link to the "Google.com"     "'
    entities = ArgEntities([MessageEntity(type='bold', offset=0, length=24)])

    with pytest.raises(ArgSyntaxEntityError):
        await SurroundedArg(TextArg())(text, 0, entities)


@pytest.mark.asyncio
async def test_surrounded_arg_wrong_after():
    text = '"Foo    bar     "'

    with pytest.raises(ArgCustomError):
        await SurroundedArg(WordArg())(text, 0, NO_ENTITIES)


@pytest.mark.parametrize('text,starts_with,result', [
    ('-> Test', '->', 'Test'),
    ('123Test', '123', 'Test'),
])
@pytest.mark.asyncio
async def test_starts_with_arg(text: str, starts_with: str, result: str):
    arg = await StartsWithArg(starts_with, WordArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(text)


@pytest.mark.parametrize('text,until,chars_before_until,result', [
    ('Foo Bar -> Foo', '->', 1, 'Foo Bar '),
    ('Foo Bar | Foo', '|', 1, 'Foo Bar '),
    ('123 321 231 -> 21311 1234', '->', 1, '123 321 231 '),
    # TODO: ('Foo Bar', '->', 1, 'Foo Bar'),
])
@pytest.mark.asyncio
async def test_until_arg(text: str, until: str, chars_before_until: int, result: str):
    arg = await UntilArg(until, TextArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    # assert arg.length == len(text)


@pytest.mark.parametrize('text,result', [
    ('foo $bar "Example text" baz baz', ('foo', 'bar', 'Example text', 'baz baz')),
    ('foo "Example text" baz baz', ('foo', None, 'Example text', 'baz baz')),
    ('foo $bar baz baz', ('foo', 'bar', None, 'baz baz')),
    ('foo $bar "Example text" baz baz', ('foo', 'bar', 'Example text', 'baz baz')),
])
@pytest.mark.asyncio
async def test_optional_surrounded_after_starts_with(text: str, result: tuple[str]):
    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        arg = await AndArg(
            first=WordArg(),
            second=OptionalArg(StartsWithArg('$', WordArg())),
            third=OptionalArg(SurroundedArg(TextArg())),
            fourth=TextArg()
        )(text, 5, NO_ENTITIES)

    assert arg.value['first'].value == result[0]
    assert arg.value['second'].value == result[1]
    assert arg.value['third'].value == result[2]
    assert arg.value['fourth'].value == result[3]
    assert arg.length == len(text)
