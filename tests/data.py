from aiogram.filters import CommandObject

from ass_tg.entities import ArgEntities

TEST_CMD = CommandObject(
    prefix='!',
    command="foo",
    args="foobar"
)
NO_ENTITIES = ArgEntities([])
