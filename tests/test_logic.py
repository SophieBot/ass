from typing import Optional

import pytest

from ass_tg.types.logic import OptionalArg, AndArg
from ass_tg.types.oneword import WordArg
from ass_tg.types.text import TextArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('Example text', 'Example'),
    ('', None)
])
@pytest.mark.asyncio
async def test_optional_word_arg(text: str, result: Optional[str]):
    arg = await OptionalArg(WordArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == (len(result) if result else 0)


@pytest.mark.parametrize('text,result', [
    ('Example text', 'Example text'),
    ('', None)
])
@pytest.mark.asyncio
async def test_middleware_optional_no_text(text: str, result: Optional[str]):
    arg = await AndArg(test=OptionalArg(TextArg()))(text, 5, NO_ENTITIES)

    assert arg.value['test'].value == result
    assert arg.value['test'].length == (len(result) if result else 0)
