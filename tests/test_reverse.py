import pytest

from ass_tg.types import TextArg, SurroundedArg
from ass_tg.types.reverse import ReverseArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,values', [
    ('Hello world!! `Dar` "Foo bar"', ('Hello world!!', 'Dar', 'Foo bar')),
    ('Hello world!!   `Dar`    "Foo bar"', ('Hello world!!', 'Dar', 'Foo bar')),
])
@pytest.mark.asyncio
async def test_reverse_arg(text: str, values: tuple[str, ...]):
    arg = await ReverseArg(
        first=TextArg(),
        second=SurroundedArg(TextArg(), prefix='`', postfix='`'),
        third=SurroundedArg(TextArg())
    )(text, 5, NO_ENTITIES)

    assert arg.value['first'].value == values[0]
    assert arg.value['second'].value == values[1]
    assert arg.value['third'].value == values[2]

    assert arg.length == len(text)
