import pytest
from aiogram.utils.i18n import I18n

from ass_tg.exceptions import ArgSimpleTypeError
from ass_tg.i18n import gettext_ctx
from ass_tg.types import TextArg, SurroundedArg
from tests.data import NO_ENTITIES


@pytest.mark.parametrize('text,result', [
    ('"Hello world"', "Hello world"),
    ('"Hello world" blahblah', "Hello world"),
    ('"Hello world"" blahblah', "Hello world"),
])
@pytest.mark.asyncio
async def test_quoted_text_arg(text: str, result: str):
    arg = await SurroundedArg(TextArg())(text, 5, NO_ENTITIES)

    assert arg.value == result
    assert arg.length == len(result) + 2  # Because of the length of the quotes


@pytest.mark.parametrize('text', [
    '"Hello world',
    'Hello world',
    'Hello world"',
    'Hello "world',
    'Hello "world"',
])
@pytest.mark.asyncio
async def test_quoted_text_arg_fail(text: str):
    arg = SurroundedArg(TextArg())

    i18n = I18n(path='/')
    gettext_ctx.set(i18n)

    with i18n.context():
        with pytest.raises(ArgSimpleTypeError):
            assert not arg.check(text, NO_ENTITIES)


@pytest.mark.parametrize('text', [
    '"Hello world',
    'Hello world',
    'Hello world"',
    'Hello "world',
    'Hello "world"',
])
@pytest.mark.asyncio
async def test_rest_text_arg(text: str):
    arg = await TextArg()(text, 3, NO_ENTITIES)

    assert arg.value == text
    assert arg.length == len(text)


@pytest.mark.parametrize('data', [
    'Hello world',
    'Foo "Bar"'
])
@pytest.mark.asyncio
async def test_text_unparse(data: str):
    assert TextArg().unparse(data) == data
